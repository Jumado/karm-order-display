Imports System.ComponentModel
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Data
Public Class frmOrderDisplay

    Dim WithEvents bgwkOrderInformation As New BackgroundWorker
    Dim WithEvents BgwkRetrieveOrders As New BackgroundWorker
    Dim WithEvents BgwkCustomerOT As New BackgroundWorker
    Dim CurrentRec As Integer = 0
    Dim SelIndex As Integer = 0
    Dim OrderDict As New Dictionary(Of Integer, String)

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub SettingsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SettingsToolStripMenuItem.Click
        frmSettings.Show()

    End Sub

    Private Sub BgwkRetrieveOrders_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BgwkRetrieveOrders.DoWork
        Try
            Dim SqlConnectionstr As New SqlConnectionStringBuilder
            SqlConnectionstr.DataSource = My.Settings.ServerName.Trim
            SqlConnectionstr.UserID = My.Settings.DBUsername.Trim
            SqlConnectionstr.InitialCatalog = My.Settings.DBName.Trim
            SqlConnectionstr.Password = My.Settings.ServerPassword.Trim
            Dim DBConnOrder As New SqlConnection(SqlConnectionstr.ToString)

            Try

                If (DBConnOrder.State = ConnectionState.Closed) Then
                    DBConnOrder.Open()
                End If
                Dim RecNum As Integer = 0
                Dim CMDOrders As New SqlCommand
                CMDOrders.Connection = DBConnOrder
                CMDOrders.CommandText = "SELECT DISTINCT  so_no ,Committ_date FROM VwOrderDisplay WHERE (BAL >=5) ORDER BY Committ_date ASC"
                CMDOrders.CommandType = CommandType.Text
                Dim Reader As SqlDataReader = CMDOrders.ExecuteReader
                While Reader.Read
                    If Not (DBNull.Value.Equals(Reader!so_no)) Then
                        RecNum += 1
                        Me.BgwkRetrieveOrders.ReportProgress(RecNum, IIf(DBNull.Value.Equals(Reader!so_no), String.Empty, Reader!so_no))
                    End If
                End While

            Catch ex As Exception
                Me.BgwkRetrieveOrders.CancelAsync()
            Finally
                If (DBConnOrder.State = ConnectionState.Closed) Then
                    DBConnOrder.Open()
                End If
            End Try
        Catch ex As Exception
        End Try
    End Sub

    Private Sub frmOrderDisplay_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.GotFocus
        Try
            Me.SuperMarqueeComplete.Running = True
        Catch ex As Exception

        End Try

    End Sub

    


    Private Sub frmOrderDisplay_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            
            Me.timScreenRefresh.Interval = CInt((My.Settings.UpdateInterval) * 60000)
        Catch ex As Exception
        Finally
            Me.BgwkRetrieveOrders.WorkerReportsProgress = True
            Me.BgwkRetrieveOrders.WorkerSupportsCancellation = True
            Me.bgwkOrderInformation.WorkerSupportsCancellation = True
            Me.bgwkOrderInformation.WorkerReportsProgress = True
            Me.BgwkCustomerOT.WorkerReportsProgress = True
            Me.BgwkCustomerOT.WorkerSupportsCancellation = True
            Me.BgwkRetrieveOrders.RunWorkerAsync()
        End Try
        
    End Sub

    Private Sub BgwkRetrieveOrders_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles BgwkRetrieveOrders.ProgressChanged
        Try
            Me.OrderDict.Add(CInt(e.ProgressPercentage), TryCast(e.UserState, String))
            
        Catch ex As Exception

        End Try
    End Sub

    Private Sub bgwkOrderInformation_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwkOrderInformation.DoWork
        Try
            Dim LiCollection As New ArrayList
            Dim SqlConnectionstr As New SqlConnectionStringBuilder
            SqlConnectionstr.DataSource = My.Settings.ServerName.Trim
            SqlConnectionstr.UserID = My.Settings.DBUsername.Trim
            SqlConnectionstr.InitialCatalog = My.Settings.DBName.Trim
            SqlConnectionstr.Password = My.Settings.ServerPassword.Trim
            Dim DBConnOrder As New SqlConnection(SqlConnectionstr.ToString)
            Try
                If (DBConnOrder.State = ConnectionState.Closed) Then
                    DBConnOrder.Open()
                End If

                Dim CMDOrderInfo As New SqlCommand
                CMDOrderInfo.Connection = DBConnOrder
                CMDOrderInfo.CommandText = "karmProcOrderDisplay"
                CMDOrderInfo.CommandType = CommandType.StoredProcedure
                CMDOrderInfo.Parameters.Clear()
                CMDOrderInfo.Parameters.AddWithValue("@OrderNo", TryCast(e.Argument, String))
                Dim Reader As SqlDataReader
                Reader = CMDOrderInfo.ExecuteReader
                Me.bgwkOrderInformation.ReportProgress(0)
                While Reader.Read
                    Dim LI As New ListViewItem
                    LI.Text = IIf(DBNull.Value.Equals(Reader!ItemCode), String.Empty, Reader!ItemCode)
                    LI.SubItems.Add(IIf(DBNull.Value.Equals(Reader!ItemDescription), String.Empty, Reader!ItemDescription))
                    LI.SubItems.Add(IIf(DBNull.Value.Equals(Reader!OrderedQty), 0.0F, Val(Reader!OrderedQty)))
                    LI.SubItems.Add(IIf(DBNull.Value.Equals(Reader!Allocated), 0.0F, Val(Reader!Allocated)))
                    LI.SubItems.Add(IIf(DBNull.Value.Equals(Reader!PalletisedQty), 0.0F, Val(Reader!PalletisedQty)))
                    LI.SubItems.Add(IIf(DBNull.Value.Equals(Reader!Balance), 0.0F, Math.Round(Val(Reader!Balance), 2)))
                    LI.Tag = IIf(DBNull.Value.Equals(Reader!Customer), String.Empty, Reader!Customer)
                    LI.SubItems.Add(IIf(DBNull.Value.Equals(Reader!BatchNum), String.Empty, Reader!BatchNum))
                    LI.SubItems.Add(IIf(DBNull.Value.Equals(Reader!Remark), String.Empty, Reader!Remark))
                    LI.SubItems.Add(IIf(DBNull.Value.Equals(Reader!Age), String.Empty, Reader!Age))
                    LI.SubItems.Add(IIf(DBNull.Value.Equals(Reader!Requestdate), String.Empty, Reader!Requestdate))
                    LI.ToolTipText = IIf(DBNull.Value.Equals(Reader!Commitdate), String.Empty, Reader!Commitdate)
                    If CInt(Val(Reader!Balance) / Me.ItemWeight(Reader!ItemCode.ToString)) > CInt(((Reader!OrderedQty) / Me.ItemWeight(Reader!ItemCode.ToString)) * 0.02) Then
                        LI.BackColor = Color.Red
                    Else
                        LI.BackColor = Color.White
                    End If
                    If (Math.Round(Val(Reader!Balance), 2) < 0) And (((Val(Reader!Balance) * -1) > 0.001)) Then
                        LI.BackColor = Color.Gold
                    End If
                    If (LI.BackColor = Color.Gold) Then
                        LI.StateImageIndex = 2
                    ElseIf (LI.BackColor = Color.Red) Then
                        LI.StateImageIndex = 1
                    ElseIf (LI.BackColor = Color.White) Then
                        LI.StateImageIndex = 0
                    End If
                    LiCollection.Add(LI)
                    LI = Nothing
                End While
                Reader.Close()
                e.Result = LiCollection


            Catch ex As Exception
                Me.bgwkOrderInformation.CancelAsync()
            Finally
                If (DBConnOrder.State = ConnectionState.Open) Then
                    DBConnOrder.Close()
                End If
            End Try
        Catch ex As Exception
        End Try

    End Sub

   

    Private Sub timCurrentTime_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles timCurrentTime.Tick
        Me.ToolStripLabelCurrentTime.Text = Now.ToString("dddd  MMMM d,yyyy" & Space(3) & "h:mm:ss tt").ToUpper
    End Sub




    Private Sub BgwkRetrieveOrders_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BgwkRetrieveOrders.RunWorkerCompleted
        Try
            Me.CurrentRec = 0
            If (Me.timScreenRefresh.Enabled = False) Then
                Me.timScreenRefresh.Enabled = True
            End If
            Me.timScreenRefresh.Start()
            If (Me.SuperMarqueeComplete.Elements.Count > 0) Then
                Me.SuperMarqueeComplete.Elements.Clear()
                Me.SuperMarqueeComplete.Invalidate()
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub timScroll_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles timScroll.Tick
        If Not (Me.bgwkOrderInformation.IsBusy Or Me.BgwkRetrieveOrders.IsBusy) Then
            If Not (Me.lstvwOrderInfo.Items.Count = 0) Then
                If (Me.SelIndex > (Me.lstvwOrderInfo.Items.Count - 1)) Then
                    Me.SelIndex = 0
                End If
                Me.lstvwOrderInfo.Items(Me.SelIndex).EnsureVisible()
                Me.SelIndex += 1
            Else
                Me.SelIndex = 0
            End If
        End If
    End Sub
    Private Function ItemWeight(ByVal ItemCode As String) As Double
        Dim Rad As Double = 0
        Dim Gaug As Double = 0
        Gaug = ItemCode.Trim.Substring(6, 3)
        If (ItemCode.Trim.Substring(0, 2).ToUpper = "CI") Then
            Rad = (Val(ItemCode.Trim.Substring(2, 4)) / 200) * 2.54
        ElseIf (ItemCode.Trim.Substring(0, 2).ToUpper = "CM") Then
            Rad = (Val(ItemCode.Substring(2, 4).Trim) / 200)
        End If
        Return (Math.PI * (Math.Pow(Rad, 2)) * (2.71) * (Math.Pow(10, -6)) * Gaug)
    End Function

    Private Sub timScreenRefresh_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles timScreenRefresh.Tick
        Try
            If (Not (Me.OrderDict.Count < Me.CurrentRec)) Then
                If Not (Me.bgwkOrderInformation.IsBusy Or Me.bgwkOrderInformation.IsBusy) Then
                    Me.CurrentRec += 1
                    Me.bgwkOrderInformation.RunWorkerAsync(OrderDict.Item(Me.CurrentRec).Trim)
                End If

            Else
                Me.OrderDict.Clear()
                If Not (Me.BgwkRetrieveOrders.IsBusy) Then
                    Me.BgwkRetrieveOrders.RunWorkerAsync()
                End If

            End If
        Catch ex As Exception

        End Try


    End Sub

    Private Sub EnableAutoupdateToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EnableAutoupdateToolStripMenuItem.Click
        If Not (Me.BgwkRetrieveOrders.IsBusy) Then
            Me.timScreenRefresh.Enabled = True
            MsgBox("Auto-update enabled", MsgBoxStyle.OkOnly + MsgBoxStyle.ApplicationModal + MsgBoxStyle.Information, "Auto-update")
            TryCast(sender, ToolStripMenuItem).Checked = True
        Else
            MsgBox("Loading orders", MsgBoxStyle.OkOnly + MsgBoxStyle.ApplicationModal + MsgBoxStyle.Information, "Please wait")

        End If
    End Sub

    Private Sub bgwkOrderInformation_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles bgwkOrderInformation.ProgressChanged
        Me.timScreenRefresh.Stop()
    End Sub

   

   


    Private Sub bgwkOrderInformation_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwkOrderInformation.RunWorkerCompleted
        Try
            If (Not (IsNothing(e.Result))) AndAlso (Not (TryCast(e.Result, ArrayList).Capacity = 0)) Then
                Me.lstvwOrderInfo.Items.Clear()
                Me.shpctrl.Text = String.Empty
                For Each ArrayItem As Object In TryCast(e.Result, ArrayList)
                    Me.lstvwOrderInfo.Items.Add(TryCast(ArrayItem, ListViewItem))
                    Me.ToolStripLabelOrder.Text = "ORD.#: " & OrderDict.Item(Me.CurrentRec).Trim & "  " & "REQ. DATE:" & Date.Parse(TryCast(ArrayItem, ListViewItem).SubItems(9).Text.Trim).ToString("dd-MM-yyyy")
                    Me.ToolStripLabelCustomer.Text = "CUST.: " & TryCast(ArrayItem, ListViewItem).Tag.ToString.ToUpper
                    If Not ((TryCast(ArrayItem, ListViewItem).SubItems(6).Text.Trim) = String.Empty) Then
                        Me.lblBatch.Text = "BATCH: " & TryCast(ArrayItem, ListViewItem).SubItems(6).Text.Trim.ToUpper
                    Else
                        Me.lblBatch.Text = String.Empty
                    End If
                    If Not ((TryCast(ArrayItem, ListViewItem).ToolTipText.Trim) = String.Empty) Then
                        Me.ToolStripLabelCommitDate.Text = "CMT DATE :" & Date.Parse(TryCast(ArrayItem, ListViewItem).ToolTipText.Trim).ToString("dd-MM-yyyy")
                    Else
                        Me.ToolStripLabelCommitDate.Text = "CMT DATE : -NONE-"
                    End If
                    If TryCast(ArrayItem, ListViewItem).SubItems(7).Text.Trim.ToUpper = "STOCK" Then
                        Me.ToolStripLabelCommitDate.Text = "CMT DATE : STOCK"
                    End If
                    If Not ((TryCast(ArrayItem, ListViewItem).SubItems(7).Text.Trim) = String.Empty) Then
                        Me.lblRemarks.Text = "REMARKS : " & TryCast(ArrayItem, ListViewItem).SubItems(7).Text.Trim.ToUpper
                    Else
                        Me.lblRemarks.Text = String.Empty
                    End If
                    If Not ((TryCast(ArrayItem, ListViewItem).SubItems(8).Text.Trim) = String.Empty) Then
                        Me.lblAge.Text = "AGE: " & TryCast(ArrayItem, ListViewItem).SubItems(8).Text.Trim
                    Else
                        Me.lblAge.Text = String.Empty
                    End If

                Next

                Dim OrderComplete As Boolean = True
                For Each LI As ListViewItem In Me.lstvwOrderInfo.Items
                    If LI.BackColor <> Color.White Then
                        OrderComplete = False
                    End If
                Next
                If (OrderComplete) And (Not (Me.lstvwOrderInfo.Items.Count = 0)) Then
                    Dim Mqr As New MarqueControl.Entity.TextElement
                    Mqr.Text = OrderDict.Item(Me.CurrentRec).Trim
                    Mqr.ForeColor = Color.White
                    Mqr.Font = New Font(Mqr.Font.Name, 28, FontStyle.Bold, Mqr.Font.Unit)
                    Mqr.RightImageIndex = 3
                    Mqr.LeftImageIndex = 3
                    If Not Me.SuperMarqueeComplete.Elements.Contains(Mqr) Then
                        Me.SuperMarqueeComplete.Elements.Add(Mqr)
                    End If
                End If

            End If
            Me.BgwkCustomerOT.RunWorkerAsync(OrderDict.Item(Me.CurrentRec).Trim)
        Catch ex As Exception


        End Try
        Me.timScreenRefresh.Start()
    End Sub

    Private Sub BgwkCustomerOT_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BgwkCustomerOT.DoWork
        Try
            Dim SqlConnectionstr As New SqlConnectionStringBuilder
            SqlConnectionstr.DataSource = My.Settings.ServerName.Trim
            SqlConnectionstr.UserID = My.Settings.DBUsername.Trim
            SqlConnectionstr.InitialCatalog = My.Settings.DBName.Trim
            SqlConnectionstr.Password = My.Settings.ServerPassword.Trim
            Dim DBConnOrder As New SqlConnection(SqlConnectionstr.ToString)
            Try
                If (DBConnOrder.State = ConnectionState.Closed) Then
                    DBConnOrder.Open()
                End If
                Dim CMDOTP As New SqlCommand
                CMDOTP.Connection = DBConnOrder
                CMDOTP.CommandText = "ProkarmOTP"
                CMDOTP.CommandType = CommandType.StoredProcedure
                CMDOTP.Parameters.Clear()
                CMDOTP.Parameters.AddWithValue("@Order", TryCast(e.Argument, String))
                Dim Reader As SqlDataReader = CMDOTP.ExecuteReader
                If Reader.HasRows Then
                    While Reader.Read
                        e.Result = Reader!Percentage
                    End While
                    Reader.Close()
                Else
                    e.Result = Convert.ToString(0.0F) + " %"
                End If
            Catch ex As Exception
                Me.BgwkCustomerOT.CancelAsync()
            Finally
                If DBConnOrder.State = ConnectionState.Open Then
                    DBConnOrder.Close()
                    DBConnOrder.Dispose()
                End If


            End Try
        Catch ex As Exception
            Me.BgwkCustomerOT.CancelAsync()
        End Try
    End Sub

    Private Sub BgwkCustomerOT_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BgwkCustomerOT.RunWorkerCompleted
        Try
            If Not (IsNothing(e.Result)) Then
                shpctrl.Text = "CUST. O.T: " & TryCast(e.Result, String)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ToolStripLabelCurrentTime_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripLabelCurrentTime.Click

    End Sub
End Class
