<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOrderDisplay
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmOrderDisplay))
        Me.cntxmnuSetup = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.SettingsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.EnableAutoupdateToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.timCurrentTime = New System.Windows.Forms.Timer(Me.components)
        Me.timScroll = New System.Windows.Forms.Timer(Me.components)
        Me.timScreenRefresh = New System.Windows.Forms.Timer(Me.components)
        Me.StatusStrip2 = New System.Windows.Forms.StatusStrip
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip
        Me.ToolStrip3 = New System.Windows.Forms.ToolStrip
        Me.ImageListSign = New System.Windows.Forms.ImageList(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.PictureBox3 = New System.Windows.Forms.PictureBox
        Me.PictureBox2 = New System.Windows.Forms.PictureBox
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.ToolStrip4 = New System.Windows.Forms.ToolStrip
        Me.ToolStripBanner = New System.Windows.Forms.ToolStrip
        Me.ToolStripLabelCustomer = New System.Windows.Forms.ToolStripLabel
        Me.ToolStripLabelOrder = New System.Windows.Forms.ToolStripLabel
        Me.ToolStripLabelCommitDate = New System.Windows.Forms.ToolStripLabel
        Me.ToolStrip2 = New System.Windows.Forms.ToolStrip
        Me.ToolStrip5 = New System.Windows.Forms.ToolStrip
        Me.ToolStrip6 = New System.Windows.Forms.ToolStrip
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.Label1 = New System.Windows.Forms.Label
        Me.SuperMarqueeComplete = New MarqueControl.Controls.SuperMarquee
        Me.shpctrl = New ShapeControl.ShapeControl
        Me.ToolStrip7 = New System.Windows.Forms.ToolStrip
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader2 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader3 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader4 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader5 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader6 = New System.Windows.Forms.ColumnHeader
        Me.lstvwOrderInfo = New System.Windows.Forms.ListView
        Me.ToolStripLabelCurrentTime = New System.Windows.Forms.ToolStripLabel
        Me.lblBatch = New System.Windows.Forms.ToolStripLabel
        Me.lblRemarks = New System.Windows.Forms.ToolStripLabel
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.lblAge = New System.Windows.Forms.ToolStripLabel
        Me.cntxmnuSetup.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStripBanner.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'cntxmnuSetup
        '
        Me.cntxmnuSetup.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SettingsToolStripMenuItem, Me.EnableAutoupdateToolStripMenuItem, Me.ExitToolStripMenuItem})
        Me.cntxmnuSetup.Name = "cntxmnuSetup"
        Me.cntxmnuSetup.Size = New System.Drawing.Size(181, 70)
        '
        'SettingsToolStripMenuItem
        '
        Me.SettingsToolStripMenuItem.Name = "SettingsToolStripMenuItem"
        Me.SettingsToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.SettingsToolStripMenuItem.Text = "Settings"
        '
        'EnableAutoupdateToolStripMenuItem
        '
        Me.EnableAutoupdateToolStripMenuItem.Name = "EnableAutoupdateToolStripMenuItem"
        Me.EnableAutoupdateToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.EnableAutoupdateToolStripMenuItem.Text = "Enable Auto-update"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'timCurrentTime
        '
        Me.timCurrentTime.Enabled = True
        Me.timCurrentTime.Interval = 1000
        '
        'timScroll
        '
        Me.timScroll.Enabled = True
        Me.timScroll.Interval = 1000
        '
        'timScreenRefresh
        '
        Me.timScreenRefresh.Interval = 60000
        '
        'StatusStrip2
        '
        Me.StatusStrip2.AutoSize = False
        Me.StatusStrip2.BackColor = System.Drawing.Color.Ivory
        Me.StatusStrip2.Dock = System.Windows.Forms.DockStyle.Left
        Me.StatusStrip2.Location = New System.Drawing.Point(0, 0)
        Me.StatusStrip2.Name = "StatusStrip2"
        Me.StatusStrip2.Size = New System.Drawing.Size(10, 514)
        Me.StatusStrip2.SizingGrip = False
        Me.StatusStrip2.TabIndex = 84
        Me.StatusStrip2.Text = "StatusStrip2"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.AutoSize = False
        Me.StatusStrip1.BackColor = System.Drawing.Color.Ivory
        Me.StatusStrip1.Dock = System.Windows.Forms.DockStyle.Right
        Me.StatusStrip1.Location = New System.Drawing.Point(942, 0)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(10, 514)
        Me.StatusStrip1.SizingGrip = False
        Me.StatusStrip1.TabIndex = 85
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolStrip3
        '
        Me.ToolStrip3.AutoSize = False
        Me.ToolStrip3.BackColor = System.Drawing.Color.Ivory
        Me.ToolStrip3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.ToolStrip3.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStrip3.Location = New System.Drawing.Point(10, 504)
        Me.ToolStrip3.Name = "ToolStrip3"
        Me.ToolStrip3.Size = New System.Drawing.Size(932, 10)
        Me.ToolStrip3.TabIndex = 87
        Me.ToolStrip3.Text = "ToolStrip3"
        '
        'ImageListSign
        '
        Me.ImageListSign.ImageStream = CType(resources.GetObject("ImageListSign.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageListSign.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageListSign.Images.SetKeyName(0, "Checked.ico")
        Me.ImageListSign.Images.SetKeyName(1, "Cancel.ico")
        Me.ImageListSign.Images.SetKeyName(2, "Warning_2.ico")
        Me.ImageListSign.Images.SetKeyName(3, "Char - Star.ico")
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.PictureBox3)
        Me.Panel1.Controls.Add(Me.PictureBox2)
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(10, 25)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(932, 110)
        Me.Panel1.TabIndex = 105
        '
        'PictureBox3
        '
        Me.PictureBox3.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.PictureBox3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PictureBox3.Image = Global.KarmOrderDisplay.My.Resources.Resources.Zoom
        Me.PictureBox3.Location = New System.Drawing.Point(168, 0)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(594, 108)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox3.TabIndex = 4
        Me.PictureBox3.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Dock = System.Windows.Forms.DockStyle.Right
        Me.PictureBox2.Image = Global.KarmOrderDisplay.My.Resources.Resources.kaluworks
        Me.PictureBox2.Location = New System.Drawing.Point(762, 0)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(168, 108)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 3
        Me.PictureBox2.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Dock = System.Windows.Forms.DockStyle.Left
        Me.PictureBox1.Image = Global.KarmOrderDisplay.My.Resources.Resources.kaluworks
        Me.PictureBox1.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(168, 108)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 2
        Me.PictureBox1.TabStop = False
        '
        'ToolStrip4
        '
        Me.ToolStrip4.AutoSize = False
        Me.ToolStrip4.BackColor = System.Drawing.Color.Ivory
        Me.ToolStrip4.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStrip4.Location = New System.Drawing.Point(10, 213)
        Me.ToolStrip4.Name = "ToolStrip4"
        Me.ToolStrip4.Size = New System.Drawing.Size(932, 10)
        Me.ToolStrip4.TabIndex = 112
        Me.ToolStrip4.Text = "ToolStrip4"
        '
        'ToolStripBanner
        '
        Me.ToolStripBanner.BackColor = System.Drawing.Color.MidnightBlue
        Me.ToolStripBanner.CanOverflow = False
        Me.ToolStripBanner.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStripBanner.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripLabelCustomer, Me.ToolStripLabelOrder, Me.ToolStripLabelCommitDate})
        Me.ToolStripBanner.Location = New System.Drawing.Point(10, 170)
        Me.ToolStripBanner.Name = "ToolStripBanner"
        Me.ToolStripBanner.Size = New System.Drawing.Size(932, 43)
        Me.ToolStripBanner.TabIndex = 111
        Me.ToolStripBanner.Text = "ToolStrip1"
        '
        'ToolStripLabelCustomer
        '
        Me.ToolStripLabelCustomer.Font = New System.Drawing.Font("Segoe UI", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripLabelCustomer.ForeColor = System.Drawing.Color.White
        Me.ToolStripLabelCustomer.Margin = New System.Windows.Forms.Padding(10, 1, 0, 2)
        Me.ToolStripLabelCustomer.Name = "ToolStripLabelCustomer"
        Me.ToolStripLabelCustomer.Size = New System.Drawing.Size(102, 40)
        Me.ToolStripLabelCustomer.Text = "CUST.:"
        '
        'ToolStripLabelOrder
        '
        Me.ToolStripLabelOrder.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripLabelOrder.Font = New System.Drawing.Font("Segoe UI", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripLabelOrder.ForeColor = System.Drawing.Color.White
        Me.ToolStripLabelOrder.Margin = New System.Windows.Forms.Padding(5, 1, 5, 2)
        Me.ToolStripLabelOrder.Name = "ToolStripLabelOrder"
        Me.ToolStripLabelOrder.Size = New System.Drawing.Size(138, 40)
        Me.ToolStripLabelOrder.Text = "ORDER #"
        '
        'ToolStripLabelCommitDate
        '
        Me.ToolStripLabelCommitDate.Font = New System.Drawing.Font("Segoe UI", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripLabelCommitDate.ForeColor = System.Drawing.Color.White
        Me.ToolStripLabelCommitDate.Margin = New System.Windows.Forms.Padding(5, 1, 0, 2)
        Me.ToolStripLabelCommitDate.Name = "ToolStripLabelCommitDate"
        Me.ToolStripLabelCommitDate.Size = New System.Drawing.Size(0, 40)
        '
        'ToolStrip2
        '
        Me.ToolStrip2.AutoSize = False
        Me.ToolStrip2.BackColor = System.Drawing.Color.Ivory
        Me.ToolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStrip2.Location = New System.Drawing.Point(10, 160)
        Me.ToolStrip2.Name = "ToolStrip2"
        Me.ToolStrip2.Size = New System.Drawing.Size(932, 10)
        Me.ToolStrip2.TabIndex = 110
        Me.ToolStrip2.Text = "ToolStrip2"
        '
        'ToolStrip5
        '
        Me.ToolStrip5.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.ToolStrip5.BackgroundImage = CType(resources.GetObject("ToolStrip5.BackgroundImage"), System.Drawing.Image)
        Me.ToolStrip5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ToolStrip5.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStrip5.Location = New System.Drawing.Point(10, 135)
        Me.ToolStrip5.Name = "ToolStrip5"
        Me.ToolStrip5.Size = New System.Drawing.Size(932, 25)
        Me.ToolStrip5.TabIndex = 109
        Me.ToolStrip5.Text = "ToolStrip5"
        '
        'ToolStrip6
        '
        Me.ToolStrip6.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.ToolStrip6.BackgroundImage = CType(resources.GetObject("ToolStrip6.BackgroundImage"), System.Drawing.Image)
        Me.ToolStrip6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ToolStrip6.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStrip6.Location = New System.Drawing.Point(10, 0)
        Me.ToolStrip6.Name = "ToolStrip6"
        Me.ToolStrip6.Size = New System.Drawing.Size(932, 25)
        Me.ToolStrip6.TabIndex = 101
        Me.ToolStrip6.Text = "ToolStrip6"
        '
        'SplitContainer1
        '
        Me.SplitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.SplitContainer1.Location = New System.Drawing.Point(10, 453)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.BackColor = System.Drawing.Color.Black
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label1)
        Me.SplitContainer1.Panel1.ForeColor = System.Drawing.SystemColors.ControlLightLight
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.BackColor = System.Drawing.Color.Black
        Me.SplitContainer1.Panel2.Controls.Add(Me.SuperMarqueeComplete)
        Me.SplitContainer1.Panel2.Controls.Add(Me.shpctrl)
        Me.SplitContainer1.Size = New System.Drawing.Size(932, 51)
        Me.SplitContainer1.SplitterDistance = 426
        Me.SplitContainer1.SplitterWidth = 2
        Me.SplitContainer1.TabIndex = 131
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Black
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(5, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(431, 42)
        Me.Label1.TabIndex = 132
        Me.Label1.Text = "COMPLETE ORDERS :"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'SuperMarqueeComplete
        '
        Me.SuperMarqueeComplete.BackColor = System.Drawing.Color.Black
        Me.SuperMarqueeComplete.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperMarqueeComplete.HoverStop = False
        Me.SuperMarqueeComplete.ImageList = Me.ImageListSign
        Me.SuperMarqueeComplete.Location = New System.Drawing.Point(0, 0)
        Me.SuperMarqueeComplete.MarqueeSpeed = 980
        Me.SuperMarqueeComplete.Name = "SuperMarqueeComplete"
        Me.SuperMarqueeComplete.Size = New System.Drawing.Size(156, 49)
        Me.SuperMarqueeComplete.TabIndex = 131
        Me.SuperMarqueeComplete.TabStop = False
        '
        'shpctrl
        '
        Me.shpctrl.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.shpctrl.BorderColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.shpctrl.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
        Me.shpctrl.BorderWidth = 3
        Me.shpctrl.CenterColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.shpctrl.Dock = System.Windows.Forms.DockStyle.Right
        Me.shpctrl.Font = New System.Drawing.Font("Arial", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.shpctrl.ForeColor = System.Drawing.Color.White
        Me.shpctrl.Location = New System.Drawing.Point(156, 0)
        Me.shpctrl.Name = "shpctrl"
        Me.shpctrl.Shape = ShapeControl.ShapeType.Rectangle
        Me.shpctrl.Size = New System.Drawing.Size(346, 49)
        Me.shpctrl.SurroundColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.shpctrl.TabIndex = 0
        Me.shpctrl.Text = "CUST. O.T :"
        Me.shpctrl.UseGradient = True
        '
        'ToolStrip7
        '
        Me.ToolStrip7.AutoSize = False
        Me.ToolStrip7.BackColor = System.Drawing.Color.Ivory
        Me.ToolStrip7.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.ToolStrip7.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStrip7.Location = New System.Drawing.Point(10, 443)
        Me.ToolStrip7.Name = "ToolStrip7"
        Me.ToolStrip7.Size = New System.Drawing.Size(932, 10)
        Me.ToolStrip7.TabIndex = 133
        Me.ToolStrip7.Text = "ToolStrip7"
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Item code"
        Me.ColumnHeader1.Width = 250
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Item Description"
        Me.ColumnHeader2.Width = 800
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Ordered"
        Me.ColumnHeader3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ColumnHeader3.Width = 105
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Allocated "
        Me.ColumnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ColumnHeader4.Width = 105
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Palletised "
        Me.ColumnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ColumnHeader5.Width = 105
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "Balance"
        Me.ColumnHeader6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ColumnHeader6.Width = 105
        '
        'lstvwOrderInfo
        '
        Me.lstvwOrderInfo.BackgroundImageTiled = True
        Me.lstvwOrderInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lstvwOrderInfo.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3, Me.ColumnHeader4, Me.ColumnHeader5, Me.ColumnHeader6})
        Me.lstvwOrderInfo.ContextMenuStrip = Me.cntxmnuSetup
        Me.lstvwOrderInfo.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lstvwOrderInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lstvwOrderInfo.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstvwOrderInfo.GridLines = True
        Me.lstvwOrderInfo.HideSelection = False
        Me.lstvwOrderInfo.Location = New System.Drawing.Point(10, 223)
        Me.lstvwOrderInfo.MultiSelect = False
        Me.lstvwOrderInfo.Name = "lstvwOrderInfo"
        Me.lstvwOrderInfo.Size = New System.Drawing.Size(932, 220)
        Me.lstvwOrderInfo.StateImageList = Me.ImageListSign
        Me.lstvwOrderInfo.TabIndex = 134
        Me.lstvwOrderInfo.UseCompatibleStateImageBehavior = False
        Me.lstvwOrderInfo.View = System.Windows.Forms.View.Details
        '
        'ToolStripLabelCurrentTime
        '
        Me.ToolStripLabelCurrentTime.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripLabelCurrentTime.BackColor = System.Drawing.Color.Transparent
        Me.ToolStripLabelCurrentTime.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripLabelCurrentTime.Font = New System.Drawing.Font("Calibri", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripLabelCurrentTime.ForeColor = System.Drawing.Color.MidnightBlue
        Me.ToolStripLabelCurrentTime.Margin = New System.Windows.Forms.Padding(0, 1, 5, 2)
        Me.ToolStripLabelCurrentTime.Name = "ToolStripLabelCurrentTime"
        Me.ToolStripLabelCurrentTime.Size = New System.Drawing.Size(371, 39)
        Me.ToolStripLabelCurrentTime.Text = "ToolStripLabelCurrentTime"
        '
        'lblBatch
        '
        Me.lblBatch.BackColor = System.Drawing.Color.Transparent
        Me.lblBatch.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.lblBatch.Font = New System.Drawing.Font("Calibri", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBatch.ForeColor = System.Drawing.Color.MidnightBlue
        Me.lblBatch.Margin = New System.Windows.Forms.Padding(10, 1, 5, 2)
        Me.lblBatch.Name = "lblBatch"
        Me.lblBatch.Size = New System.Drawing.Size(127, 39)
        Me.lblBatch.Text = "BATCH #"
        '
        'lblRemarks
        '
        Me.lblRemarks.BackColor = System.Drawing.Color.Transparent
        Me.lblRemarks.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.lblRemarks.Font = New System.Drawing.Font("Calibri", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemarks.ForeColor = System.Drawing.Color.MidnightBlue
        Me.lblRemarks.Margin = New System.Windows.Forms.Padding(5, 1, 5, 2)
        Me.lblRemarks.Name = "lblRemarks"
        Me.lblRemarks.Size = New System.Drawing.Size(0, 39)
        '
        'ToolStrip1
        '
        Me.ToolStrip1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.ToolStrip1.BackgroundImage = CType(resources.GetObject("ToolStrip1.BackgroundImage"), System.Drawing.Image)
        Me.ToolStrip1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ToolStrip1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.ToolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripLabelCurrentTime, Me.lblBatch, Me.lblRemarks, Me.lblAge})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 514)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(952, 42)
        Me.ToolStrip1.TabIndex = 21
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'lblAge
        '
        Me.lblAge.BackColor = System.Drawing.Color.Transparent
        Me.lblAge.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.lblAge.Font = New System.Drawing.Font("Calibri", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAge.ForeColor = System.Drawing.Color.MidnightBlue
        Me.lblAge.Margin = New System.Windows.Forms.Padding(10, 1, 5, 2)
        Me.lblAge.Name = "lblAge"
        Me.lblAge.Size = New System.Drawing.Size(0, 39)
        '
        'frmOrderDisplay
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.ClientSize = New System.Drawing.Size(952, 556)
        Me.ControlBox = False
        Me.Controls.Add(Me.lstvwOrderInfo)
        Me.Controls.Add(Me.ToolStrip7)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.ToolStrip4)
        Me.Controls.Add(Me.ToolStripBanner)
        Me.Controls.Add(Me.ToolStrip2)
        Me.Controls.Add(Me.ToolStrip5)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.ToolStrip6)
        Me.Controls.Add(Me.ToolStrip3)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.StatusStrip2)
        Me.Controls.Add(Me.ToolStrip1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmOrderDisplay"
        Me.ShowIcon = False
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.cntxmnuSetup.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStripBanner.ResumeLayout(False)
        Me.ToolStripBanner.PerformLayout()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cntxmnuSetup As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents SettingsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents timCurrentTime As System.Windows.Forms.Timer
    Friend WithEvents timScroll As System.Windows.Forms.Timer
    Friend WithEvents timScreenRefresh As System.Windows.Forms.Timer
    Friend WithEvents EnableAutoupdateToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StatusStrip2 As System.Windows.Forms.StatusStrip
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStrip3 As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStrip6 As System.Windows.Forms.ToolStrip
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents ToolStrip5 As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStrip4 As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStripBanner As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStripLabelCustomer As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripLabelOrder As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripLabelCommitDate As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStrip2 As System.Windows.Forms.ToolStrip
    Friend WithEvents ImageListSign As System.Windows.Forms.ImageList
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents ToolStrip7 As System.Windows.Forms.ToolStrip
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader6 As System.Windows.Forms.ColumnHeader
    Friend WithEvents lstvwOrderInfo As System.Windows.Forms.ListView
    Friend WithEvents ToolStripLabelCurrentTime As System.Windows.Forms.ToolStripLabel
    Friend WithEvents lblBatch As System.Windows.Forms.ToolStripLabel
    Friend WithEvents lblRemarks As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents lblAge As System.Windows.Forms.ToolStripLabel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents SuperMarqueeComplete As MarqueControl.Controls.SuperMarquee
    Friend WithEvents shpctrl As ShapeControl.ShapeControl

End Class
