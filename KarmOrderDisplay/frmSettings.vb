Imports System.Data.SqlClient
Public Class frmSettings
   

    Private Sub BtnDiscard_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnDiscard.Click

        Me.Close()
    End Sub


    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Try
            If (Me.txtservername.Text.Trim = String.Empty) Then
                MsgBox("Server name", MsgBoxStyle.Question + MsgBoxStyle.OkOnly + MsgBoxStyle.ApplicationModal, "Setup form")
                Me.txtservername.Select()
                Exit Sub
            End If
            If (Me.txtDatabase.Text.Trim = String.Empty) Then
                MsgBox("Database name", MsgBoxStyle.Question + MsgBoxStyle.OkOnly + MsgBoxStyle.ApplicationModal, "Setup form")
                Me.txtDatabase.Select()
                Exit Sub
            End If
            If (Me.txtDBUsername.Text.Trim = String.Empty) Then
                MsgBox("Database username", MsgBoxStyle.Question + MsgBoxStyle.OkOnly + MsgBoxStyle.ApplicationModal, "Setup form")
                Me.txtDBUsername.Select()
                Exit Sub
            End If
            If (Me.txtDBPassword.Text.Trim = String.Empty) Then
                MsgBox("Server password", MsgBoxStyle.Question + MsgBoxStyle.OkOnly + MsgBoxStyle.ApplicationModal, "Setup form")
                Me.txtDBPassword.Select()
                Exit Sub
            End If
            My.Settings.ServerName = Me.txtservername.Text.Trim
            My.Settings.ServerPassword = Me.txtDBPassword.Text.Trim
            My.Settings.DBUsername = Me.txtDBUsername.Text.Trim
            My.Settings.DBName = Me.txtDatabase.Text.Trim
            My.Settings.UpdateInterval = CInt(Me.numupdwnTimer.Value)
            My.Settings.Save()
            My.Settings.Reload()
            MsgBox("Settings updated successfully", MsgBoxStyle.Information + MsgBoxStyle.OkOnly + MsgBoxStyle.ApplicationModal, "Setup form")
            Me.Close()
        Catch ex As Exception
            MsgBox(Err.GetException.Message, MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly + MsgBoxStyle.ApplicationModal, "Setup form")
            Me.txtDBPassword.Clear()
            Me.txtDBUsername.Clear()
            Me.txtservername.Clear()
            Me.txtDatabase.Clear()
            Me.numupdwnTimer.Value = 1
        End Try
    End Sub

    Private Sub frmSettings_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try

            My.Settings.Reload()
            frmOrderDisplay.Show()
        Catch ex As Exception

        End Try
        
    End Sub
   
    Private Sub frmSettings_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        frmOrderDisplay.Close()
    End Sub
End Class